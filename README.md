# Nith's Unified Updater
My personal updater cause I use different distros and my .basrhc is getting way too big.<br>
I just like to write scripts.

## Features
- Automatically recognizes your distribution
- Updates core packages with the built-in package manager
- Updates AUR packages
- Updates flatpaks
- Updates via pip
- Updates via brew
- Updates can be done without confirming

## Supported distributions
- Debian Family (apt)
- Fedora (dnf)
- CentOS (yum) (Untested)
- OpenSuSE (zypper) (Untested)
- Arch Linux (pacman & yay)
- Void Linux (xbps) (Untested)

## How it works
```
nuu [options]

If no option was given, only the core packages will be updated

Options:
--help, -h        Print help
--version, -V     Print version
--yay             Updates AUR with yay
--flatpak         Updates core & flatpaks
--pip             Updates core & via pip
--brew            Updates core & via brew
--all, -a         Updates core + flatpaks + pip + brew
--noconfirm, -y   Assumes "yes"
--reboot          Reboot machine after updates are done
--shutdown        Shutdown machine after updates are done
```
